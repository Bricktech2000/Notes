# Gross Domestic Product

> A country’s GDP is the market value of all final goods and services produced within a country in a given period of time

see [[economy]], [[productivity]]

it is thought to be the best measure of a society’s economic well-being

it is the most closely watched [[economics]] statistic

the GPD measures both income and expenditure at once (which works since they are equal, see [[economy]])

### acronym

GDP

## Measuring the GDP

the GPD includes:

- all items produced in the economy and sold legally in the markets
- the market value of the housing services provided by the economy’s stock of housing
- both tangible goods (food, clothing, cars) and intangible services (haircuts, housecleaning, dentist visits) (see [[goods-and-services]])
- only [[goods-and-services]] that are currently produced (for instance, buying a used car would not count towards a country’s GDP)
- only [[goods-and-services]] within the geographic area of a country (not the citizenship of workers or owners, for instance)

**note**: some products are excluded due to a difficult measurement

the GDP measures all items above within a specific time interval, usually one year or one quarter

## Components of Expenditure (of the GDP)

> The GDP is the total of all value added created in an economy. The value added means the value of goods and services that have been produced minus the value of the goods and services needed to produce them, the so called intermediate consumption. &mdash; Zoom Chat

GDP = Consumption + Investment + Government Purchases + Net Exports

> **Consumption**: spending by households on good and services, with the exception of purchases of new housing

> **Investment**: spending on capital equipment, inventories, and structures, including household purchases of new housing

> **Government Purchases**: spending on [[goods-and-services]] by local, territorial, provincial, and federal governments

> **Net Exports**: the value of a nation’s exports minus the value of its imports (also called the _trade balance_)

## Real VS Nominal GDP

if total spending rises from one year to the next, one of two things must be true

- the [[economy]] is producing a larger amount of [[goods-and-services]]
- [[goods-and-services]] are being sold at higher prices

in [[economics]], we want to separate these two effects using the **Real GDP**

> **Nominal GDP**: the production of [[goods-and-services]] valued at **current prices**

> **Real GDP**: the production of [[goods-and-services]] valued at **constant prices** \*\*(adjusted for [[inflation]], which is a better measure of economic well-being)

## GDP is not perfect

here are a examples of items contributing to well-being that are left out of GDP:

- leisure as a whole
- activities that take place outside the markets
- the quality of the environment and environmental depletion
- the distribution of income

## GDP Deflator

> **GDP Deflator**: a measure of the price level as the ratio of nominal GDP to real GDP

GDP Deflator = Nominal GDP / Real GDP

the GDP Deflator can be used to calculate [[inflation]] rates
