# Inflection Point

see [[extremum]], [[classical-math-notation]]

> an inflection point or point of inflection, is a point on a smooth plane curve at which the curvature changes sign &mdash; Wikipedia

## definition

$f''(x) = 0$

## properties

[[concavity]] changes around an inflection point
