# Density

symbol: $\rho$

see [[classical-math-notation]]

## Formula

$\rho = \frac{m}{V}$, where

$\rho$ is the density of a substance

$m$ is the mass of the substance

$V$ is the volume of the substance

## Constants

$\rho_{H_2O_{4^\circ C}} = 1\frac{g}{mL}$
