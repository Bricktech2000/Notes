# OOP

_Object-Oriented Programming_

the goal of OOP is to build objects based on their real-world properties, in contrast to functional programming or to procedural programming

see [[object]], [[class]]

## Pillars of OOP

&mdash; Google and Grepper

[[abstraction]]

[[encapsulation]]

[[inheritance]]

[[polymorphism]]

## Principles of OOP

&mdash; Google and Grepper

### Single Responsibility Principle (SRP)

_every module, class or function in a computer program should have responsibility for a single part of that program's functionality_

### Open-Closed Principle (OCP)

_software entities (classes, modules, functions, etc.) should be open for extension but closed for modification_

### Liskov Substitution Principle (LSP)

_objects of a superclass shall be replaceable with objects of its subclasses without breaking the application_

### Interface Segregation Principle (ISP)

_Clients should not be forced to depend upon interfaces that they do not use_

### Dependency Inversion Principle (DIP)

_high-level modules should not depend on low-level modules; both should depend on abstractions_
