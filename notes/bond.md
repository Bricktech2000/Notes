# Bond

see [[orbital]], [[quantum-numbers]]

> A chemical bond is a lasting attraction between atoms, ions or molecules that enables the formation of chemical compounds. &mdash; Wikipedia

## sizes of bonds

a triple bond is strong than a double bond, which is stronger than a single bond. similarly, a triple bond is shorter than a double bond, which is shorter than a single bond.

## notation

bonds between atoms $A$ and $B$ are represented as follows in a [[lewis-structure]]

1. $A - B$ ($A : B$) &mdash; Single Bond
2. $A = B$ &mdash; Double Bond
3. $A \equiv B$ &mdash; Triple Bond

## [[type-of-bond]]s
