# Informal Writing

# The Unofficial Noncomprehensive Informal Writing Handbook

I like this title

## common [[informal-writing]] practices

### capital letters

capital letters are not used at the beginning of sentences. a sentence written entirely in capital letters indicates shouting

### question marks

_indicate the rise of the writer’s tone_

question marks are not required at the end of a question. question marks can be added at the end of non-question sentences

### full stops

_indicate seriousness_

full stops are not used at the end of sentences

### asterisk

_indicates emphasis or a correction_

when used around a word or a group of words, asterisks indicate emphasis. when used at the beginning of another message, an asterisk communicates a correction in the previous message

### sentences

longer sentences are often cut up into multiple shorter messages, and individual sentences are often communicated by sending multiple messages. multiple sentences can also be written one after the other with no [[punctuation]] if they can be orally read without a pause.

### [[comma-splice]]s

_indicate two related sentences_

[[comma-splice]]s are valid in [[informal-writing]] for use between two related sentences. semi colons and [[dash]]es are almost never used. commas are not mandatory for [[comma-splice]]s, which can lead to an [[informal-writing]] construct known as a _space splice_

### quotation marks

quotation marks are almost never used in [[informal-writing]]. they should not be used for emphasis
