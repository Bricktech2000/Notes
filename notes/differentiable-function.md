# Differentiable Function

see [[classical-math-notation]]

> a function is differentiable at $a$ if $f'(a)$ exists

[[calculating-a-derivative]]
