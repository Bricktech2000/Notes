# Dot Product

see [[classical-math-notation]]

the dot product is defined for any [[vector-in-rn]]

### definition

$a \cdot b = \sum_{i = 1}^{n}a_ib_i$

$a \cdot b = |a||b|\cos \theta$, where

$\theta$ is the angle between $a$ and $b$

_If vectors are identified with row matrices, the dot product can also be written as a [[matrix]] Product_ &mdash; Wikipedia

$a \cdot b = ab^\intercal$

### examples

$[3, 2, -1] \cdot [4, -6, 3] = 3 \times 4 + 2 \times -6 + -1 \times 3 = -3$

$\begin{bmatrix} 3 & 2 & -1\end{bmatrix} \cdot \begin{bmatrix} 4 & -6 & 3\end{bmatrix} = \begin{bmatrix} 3 & 2 & -1\end{bmatrix} \begin{bmatrix} 4 \\ -6 \\ 3\end{bmatrix} = -3$

### properties

the properties of dot products are the same as the properties of multiplication
