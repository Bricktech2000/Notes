# e

_mathematical constant_

see [[classical-math-notation]]

## definitions

$\lim_{n \to \infty}(1 + \frac{1}{n})^n = e$

$\lim_{n \to 0}(1 + n)^{1 / n} = e$

$\frac{d}{dx} e^x = e^x$

## example

see [[calculating-a-derivative]]

$g(x) = ex^2 + 2e^x + xe^2 + x^{e^2}$

$g'(x) = 2ex + 2e^x + e^2 + e^2x^{e^1 - 1}$
