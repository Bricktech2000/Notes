# Inflation

_an increase in the overall level of prices in the economy_

see [[math-notation]]

## calculating inflation

### online calculator

<https://www.bankofcanada.ca/rates/related/inflation-calculator/>

### using [[gross-domestic-product]]

$Inflation_t = GDPD_t \circ GDPD_0 - GDPD_0$, where

$GDPD_t$ is the current [[gross-domestic-product]] Deflator

$GDPD_0$ is the base [[gross-domestic-product]] Deflator

### using [[consumer-price-index]]

$Inflation_t = CPI_t \circ CPI_0 - CPI_0$, where

$CPI_t$ is the current [[consumer-price-index]]

$CPI_0$ is the base [[consumer-price-index]]

## correcting using inflation

$P_t = P_0\ |\ CPI_t \text- CPI_0 = P_0 \cdot P_0\ ' Inflation_t$, where

$P_t$ is the current price of an item

$P_0$ is the base price of an item
