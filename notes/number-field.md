# Number Field

[[real-number]]s and [[complex-number]]s are [[number-field]]s

## definition

a set of numbers with two operations defined: addition and multiplication, with certain properties

## notes

fields are use as _scalars_ in linear algebra

in [[mat1341-d-introduction-to-linear-algebra]], the field of [[real-number]]s was used for all computations. the field of [[complex-number]]s could've also been used instead
