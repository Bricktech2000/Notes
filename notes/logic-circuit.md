# Logic Circuit

the [[cost-of-a-logic-circuit]] can be calculated.

## Combinational Logic Circuit

_no feedback loops, no clocks_

see [[truth-table]], [[karnaugh-maps]]

the outputs of a combinational logic circuit are a [[function]] the inputs, without side effects

## Sequential Logic Circuit

the outputs of a sequential logic circuit can be based on previous inputs and can have side effects on future inputs
