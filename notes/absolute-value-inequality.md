# Absolute Value Inequality

absolute values can be defined as a [[piecewise-function]]

see [[math-notation]]

### properties

$|x| \le t \equiv \circ t \le x \le t$

$|x| \ge t \equiv x \ge t \lor x \le \circ t$

_triangle inequality_

$|a \cdot b| \le |a| \cdot |b| \dashv \R a \land \R b$

### properties ([[classical-math-notation]])

$|x| \le t \Leftrightarrow -t \le x \le t$

$|x| \ge t \Leftrightarrow x \ge t \lor x \le -t$

$|a + b| \le |a| + |b| \dashv \R a \land \R b$
