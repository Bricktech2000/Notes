# Inverse Function

## definition

let $f x$ be a function

if $y = f x \land x = f^- y$, then $f^-$ is the inverse of $f$

## properties

$f f^- x = x$

$f ^- f x = x$

the graph of $y = f^- x$$y = f x$ are symmetric about the line $y = x$
