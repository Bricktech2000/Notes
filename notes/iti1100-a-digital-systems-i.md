# ITI1100 [A] Digital Systems I

<https://uottawa.brightspace.com/d2l/le/content/285255/Home>

## general

prof name: Wassim El Ahmar

prof email: <welah096@uottawa.ca>

prof office hours: Every Friday from 15:00 to 16:00, via Zoom

## Grading Scheme

- LAB &mdash; 10% (5 labs, each worth 2%)
- AST &mdash; 30% (4 assignments, each worth 7.5%)
- MTERM &mdash; 20% (date TBD)
- FINAL &mdash; 40% (date will be set by faculty)

late submissions: -20% for every day. mark of 0 after 3 days (72 hours)

## Labs

labs will be performed in groups of two (thank god I’m with Kiera)

completion of all labs is **actually** mandatory

> A student who fails to submit a lab report for any of the 5 labs will receive EIN as a final grade. &mdash; syllabus

[[lab3]]

[[lab5]]

## textbook

Available at the uOttawa bookstore
Title: Digital Design
Authors: M Morris Mano & Michael D. Ciletti
Edition: Sixth Edition
Publisher: Pearson-Prentice Hall

&mdash; syllabus

## Class Notes

[[digital-system]], [[cost-of-a-logic-circuit]], [[logic-circuit]]

[[positional-numbering-system]]

[[binary]]

[[complement]]

[[boolean-algebra]], [[boolean-variable]]s, [[boolean-operation]]s, [[boolean-function]]s, [[boolean-expression]]

**related**

[[disjunctive-normal-form]]

[[truth-table]], [[karnaugh-maps]]

[[proof]]
