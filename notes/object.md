# Object

_an instance of a [[class]]_

see [[oop]]

Objects in [[oop]] consist of data [[abstraction]] (properties) and procedural [[abstraction]] (methods)

an Object has a state, whereas a [[class]] does not
