# Procrastination

_it isn’t a problem; it’s a solution_ &mdash; <https://youtu.be/DMw8G3RPWrQ?t=9715>

procrastination is an umbrella term, an emergent phenomenon that’s based on all kinds of different processes &mdash; <https://youtu.be/DMw8G3RPWrQ?t=2653>

### example

our brain has a circuit that tries to figure out how to be efficient. however, perfection is almost never the most energy-efficient solution to a problem.

**procrastination for studying before a test**: _why would I spend 30 hours over a full month studying to get an A where I could spend 5 hours studying the night before to get a B_
