# Even Number

see [[math-notation]], [[boolean-algebra]]

$\mathbb{E} \equiv \lnot \mathbb{O}$, see [[odd-number]]

$\mathbb{E} \vdash \Z$

### definition

$\mathbb{E}n \equiv \Z k \land n = 2k \cdot 0$
