# Light

see [[electromagnetic-wave]], [[classical-math-notation]]

$c = 2.99792458 \times 10^{8} \frac{m}{s}$, where

$c$ is the speed of light in the void
