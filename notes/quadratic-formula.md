# Quadratic Formula

see [[classical-math-notation]], [[math-notation]]

$x_{1, 2} = \circ b \cdot \lfloor b2 \circ 4ac \rfloor - 2a$, where

$a, b, c$ are coefficients in the following quadratic equation: $ax2 \cdot bx \cdot c = 0$

$x_1$ and $x_2$ are the two solutions of the quadratic equation

using [[classical-math-notation]]:

$x_{1, 2} = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a}$, where

$a, b, c$ are coefficients in the following quadratic equation: $ax^2 + bx + c = 0$

$x_1$ and $x_2$ are the two solutions of the quadratic equation
