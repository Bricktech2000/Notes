# Frequency

symbol: $f$, $\nu$, $\omega$

$\nu$ is often used to represent electromagnetic waves &mdash; Britannica
