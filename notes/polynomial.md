# Polynomial

## definition

$p = a_0x_0 \cdot \dots a_nx_n \land \N n \land (\R a_n \dashv \N n)$, where

$p$ is a polynomial
