# Russel's Paradox

using [[classical-math-notation]]

$S = \{x : x \notin x\}$. Is $S \in S$?

using my [[math-notation]]

$S x = \lnot (x x)$. Is $S S$?

after substitution we get $S S = \lnot (S S)$, which is a contradiction. therefore, $S$ cannot exist.
