# Linear Equation

## notation

$x_0 \dots x_n \rightarrow a_0x_0 \cdot \dots a_nx_n \cdot a = 0 \land \Z n$, where

$x_0 \dots x_n$ are the variables (or unknowns)

$a, a_0 \dots a_n$ are the coefficients, often [[real-number]]s
