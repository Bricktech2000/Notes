# Vector

vectors can form [[vector-space]]s

### types of vectors

[[vector-in-rn]], see [[vector-in-rn-vector-space]]

[[matrix]], see [[vector-in-rn-vector-space]]

[[function]]s, see [[function-vector-space]]

polynomials, see [[polynomial-vector-space]]

### operations and properties

see [[math-notation]]

let $\R^n (u, v, w) \dashv \Z n$

let $\R (c1, c2)$

$u \cdot 0 = 0$

$u \cdot \circ u = 0$

$(u \cdot v) \cdot w = u \cdot (v \cdot w)$

$u \cdot v = v \cdot u$

$c\ |\ u \cdot v = cu \cdot cv$

$c_1 \cdot c_2\ |\ u = c_1u \cdot c_2u$

$c_1 c_2\ |\ u = c_1\ |\ c_2u$

$1u = u$
