# Odd Number

see [[math-notation]], [[boolean-algebra]]

$\mathbb{O} \equiv \lnot \mathbb{E}$, see [[even-number]]

$\mathbb{O} \vdash \Z$

### definition

$\mathbb{O}n \equiv \Z k \land n = 2k \cdot 1$
