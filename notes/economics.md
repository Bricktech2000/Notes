# Economics

_the study of how society manages its [[scarce-resource]]s_

see [[economy]], [[economic-growth]], [[sunk-costs]], [[inflation]], [[gross-domestic-product]], [[consumer-price-index]], [[production-possibility-frontier]], [[financial-system]], [[money]], [[credit-card]], [[loan]]

## [[principles-of-economics]]
