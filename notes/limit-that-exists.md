# Limit that Exists

see [[classical-math-notation]]

$\lim_{x \to a}$ doesn't exist if

- $\lim_{x \to a+} \ne \lim_{x \to a-}$
- $x$ is on a vertical asymptote
- wild behavior (not a technical term), i. e. $\lim_{x \to 0} sin(\frac{\pi}{x})$

if $g(a) = 0$, then $\lim_{x \to a} \frac{f(x)}{g(x)}$

- does not exist if $f(a) \ne 0$
- can exist if $f(a) = 0$ (simplify first using the [[limit-rules]], and then study the limit)
