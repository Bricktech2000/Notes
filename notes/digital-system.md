# Digital System

_digital systems_ and _analog systems_ are children of _electronic systems_

a digital system is **not** necessarily [[binary]]

digital systems often store negative numbers in their Radix [[complement]] forms, but can also use [[sign-magnitude-notation]].
