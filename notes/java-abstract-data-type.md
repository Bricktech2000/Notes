# Java Abstract Data Type

## examples

- stacks
- queues
- lists

abstract data types can be implemented through Interfaces

> in [[java]], an **interface** is an abstract type that is used to specify what behavior a [[class]] should implement. interfaces may only contain abstract method signatures and constant declarations.
