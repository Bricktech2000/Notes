# Energy

symbol: $E$

see [[classical-math-notation]]

## Kinetic Energy

symbol: $KE$

$KE = \frac{1}{2}mv^2$, where

$KE$ is the kinetic energy of a moving body

$m$ is the mass of the moving body

$v$ is the velocity of the moving body
