# Java Primitive

### All Primitive types

&mdash; <https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html>

```java
byte, short, int, long, float, double, boolean, char
```

### notes

the size of booleans can be either 1 bit or 8 bits, depending on the VM implementation

primitive types are stored on the stack

primitives can be passed by reference using [[java-wrapper]]s
