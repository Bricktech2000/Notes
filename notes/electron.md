# Electron

[[classical-math-notation]]

symbol: $e^-$

$m_{e^-} = 9.10938188 \times 10^{-31} kg$
