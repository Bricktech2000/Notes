# Integer

see [[math-notation]]

## definition

_a whole number_

$\Z n \equiv \N n \lor \N (\circ n)$
