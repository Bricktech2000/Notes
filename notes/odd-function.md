# Odd Function

see [[even-function]], [[math-notation]]

## definition

_an odd function is symmetrical about the y axis, but also flipped about the x axis_

$\circ f x = f (\circ x) \dashv \R x$
