# Polynomial Vector Space

see [[vector-space]], [[math-notation]], [[polynomial]]

### properties

$(\mathbb{P}^n \dashv \mathbb{F}) \dashv \N n$

### notation

$\mathbb{P}^n$ in my [[math-notation]]

$P_n(\R)$ in [[classical-math-notation]]

### definition

$\mathbb{P}^n p \equiv p \vdash Span\{[x] n \dots [x] 0\} \dashv \N n$

## zero vector

see [[function-vector-space]]

## addition

see [[function-vector-space]]

## multiplication by scalar

see [[function-vector-space]]
