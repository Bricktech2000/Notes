# Function Vector Space

see [[vector-space]], [[math-notation]], [[function]]

## definition

$f \vdash \mathbb{F}$ if $f$ is defined on its whole domain

### counterexample

if $fx = 1 - x$, then $\lnot f \vdash \mathbb{F}$ as $1 - x = \varnothing \dashv x = 0$

## zero vector

$O x = 0 \dashv \R x$

## addition

$(f \cdot g) x = fx \cdot gx$

## multiplication by scalar

$(cf) x = c\ |\ f x$
