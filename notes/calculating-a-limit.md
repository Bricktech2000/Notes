# Calculating a Limit

see [[classical-math-notation]], [[limit-rules]]

## tricks

- $\frac{\infty}{\infty}$ indeterminate form (see [[rational-function]])
  - formal method: factor out the highest power in both the numerator and the denominator
  - informal method: look at the highest degree of the numerator and of the denominator
- addition or subtraction of fractions, try to use a common denominator
- subtraction of square roots, try to multiply by the conjugate
