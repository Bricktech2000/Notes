# Java Garbage Collector

in [[java]], memory is freed by the Garbage Collector when all references to an object are deleted. this means that memory leaks are still possible when references are not explicitly deleted.
