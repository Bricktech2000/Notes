# Boolean Algebra

_algebra using boolean variables and logical operators_

see [[math-notation]], [[classical-math-notation]], [[boolean-expression]]

## [[boolean-variable]]

## [[boolean-operation]]

## [[boolean-function]]

## [[truth-table]] to [[boolean-expression]]

_mainly for [[iti1100 a digital systems i]]_

see [[disjunctive-normal-form]]
