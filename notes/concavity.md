# Concavity

see [[classical-math-notation]]

## definition

$f(x)$ is _concave up_ if $f''(x) < 0$, it _bends upwards_

$f(x)$ is _concave down_ if $f''(x) > 0$, it _bends downwards_

## properties

A point where the concavity changes (from up to down or down to up) is an [[inflection-point]]
