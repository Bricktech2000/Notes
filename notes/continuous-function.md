# Continuous Function

see [[classical-math-notation]]

> a function is continuous at $a$ when $\lim_{x \to a} f(x) = f(a)$. both must be [[limit-that-exists]].

> a function is continuous **from the left** at $a$ when $\lim_{x \to a^-}f(x) = f(a)$. both must be [[limit-that-exists]]

> a function is continuous **from the right** at $a$ when $\lim_{x \to a^+}f(x) = f(a)$. both must be [[limit-that-exists]]

## theorem

if $f(x)$ and $g(x)$ are continuous at $a$, then the following functions are also continuous at $a$:

- $f \pm g$
- $f g$
- $c f$ ($c$ is a constant)
- $\frac f g$ if $g(a) \ne 0$
