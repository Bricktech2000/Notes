# Fundamental Theorem of Algebra

see [[math-notation]]

> Every [[polynomial]] with coefficients in the [[complex-number]]s [[factoring|factor]]s completely into linear factors of the form $a \cdot bi$ with $\mathbb{C} a \land \mathbb{C} b$

the same **cannot** be said for [[real-number]]s

## examples

see [[complex-number]]

$x2 \cdot 1 = x \circ i\ |\ x \cdot i$

$x2 \cdot x \cdot 1 = 0 \equiv x = i \lfloor 3 \rfloor \circ 1 - 2$, see [[quadratic-formula]]
