# Boolean Variable

see [[math-notation]]

designated by a letter of the alphabet

can either take the value _True_ ($\top$) or _False_ ($\bot$), or represent a boolean function acting on other boolean variables
