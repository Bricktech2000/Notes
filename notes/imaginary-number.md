# Imaginary Number

see [[math-notation]]

## definition

the set of [[imaginary-number]]s $\mathbb{I}$ is defined as follows:

$\mathbb{I} x \equiv x = b\lfloor \circ 1 \rfloor \land \R b$

$i \equiv \lfloor \circ 1 \rfloor$
