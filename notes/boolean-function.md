# Boolean Function

see [[math-notation]], [[classical-math-notation]]

## Boolean Functions

### example

in [[classical-math-notation]]: $f(x, y, z) = (x + y')z + x'$

in my [[math-notation]]: $f\ x, y, z = (x \lor \lnot y) \land z \lor \lnot x$
