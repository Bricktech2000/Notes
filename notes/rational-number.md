# Rational Number

see [[math-notation]]

$\mathbb{Q} \vdash \R$

## definition

the set of [[rational-number]]s $\mathbb{Q}$ is defined as follows:

$\mathbb{Q} x \equiv x = a \text- b \land \Z a \land \Z b$
