# Vector in Rn Vector Space

see [[vector-space]], [[math-notation]], [[vector-in-rn]]

## definition

_a vector space of [[vector-in-rn]]_

## zero vector

see [[vector-in-rn]]

## addition

see [[vector-in-rn]]

## multiplication by scalar

see [[vector-in-rn]]
