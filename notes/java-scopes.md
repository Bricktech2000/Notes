# Java Scopes

Java scopes are defined using `{ /*...*/ }`

### example

```java
{
	int k = 0;
}
k++; // throws an error
```
