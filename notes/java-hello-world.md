# Java Hello World

```java
public class HelloWorld {
  public static void main(String[] args) {
    System.out.println("Hello, World");
  }
}
```

### compilation and execution

```bash
java HelloWorld.java
```
