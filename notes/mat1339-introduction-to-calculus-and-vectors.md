# MAT1339 Introduction to Calculus and Vectors

## general

prof name: Mahdi Ammar

## Homework

homework is really important for this class &mdash; discord

TA email for homework-related questions: <atalekar@uottawa.ca> &mdash; DGD

## Textbook

Main textbook (required): “Introduction to Computing Using Python: An Application Development Focus, 2nd Edition” by LjubomirPerkovic

- Paper version at uOttawa bookstore or order online eBook online ($48.00 or $17 eBook rental for 1 semester)
- https://www.wiley.com/en-ca/Introduction+to+Computing+Using+Python%3A+An+Application+Development+Focus%2C+2nd+Edition-p-9781118890943
- link for textbook &mdash; discord general: <https://libgen.is/book/index.php?md5=7016A2960349062F4E1319315D512E8C> (<https://www.ilc.org/products/mcv4u-calculus-and-vectors-online-course>)

## Class Notes

[[optimizing-a-function]]

[[limit-that-exists]]

[[calculating-a-limit]]

[[calculating-a-derivative]]

[[even-function]] and [[odd function]]

critical points - [[inflection-point]] [[extremum]], [[concavity]]

[[vector-in-rn]]

[[plane-in-r3]], [[line-in-r3]]

**related**

[[e]]

[[rational-function]]

[[piecewise-function]]

[[continuous-function]]
