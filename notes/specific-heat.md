# Specific Heat

see [[classical-math-notation]]

## Formula

$Q = mc\Delta T$, where

$Q$ is the potential [[energy]] stored in the form of heat, see [[electromagnetic-wave]]

$m$ is the mass of the body

$c$ is the specific heat of the body

$\Delta T$ is the change in temperature of the body

## Constants

$c_{H_2O} =4.184 \frac{J}{g^\circ C}$
