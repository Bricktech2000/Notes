# Money

see [[wealth]], [[bank]]

[[money]] is one of the assets with highest [[liquidity]]

> **money** is the set of assets in the [[economy]] that people regularly use to buy [[goods-and-services]] from other people

### functions of money

money is:

> a **medium of exchange**, an item that buyers give to sellers when they want to purchase [[goods-and-services]]

> a **unit of account**, a yardstick people use to post prices and record debts

> a **store of value**, an item that people can use to transfer purchasing power from the present to the future

### types of money

> **commodity money** is [[money]] that takes the form of a commodity with intrinsic value

> **fiat money** is [[money]] without intrinsic value that is accepted because of government decree

### definitions

> the **money stock** or **money supply** is the amount of [[money]] circulating in the [[economy]]

> **currency** is the paper bills and coins in the hands of the public

> the **monetary policy** is the setting of the money supply by policymakers in the central bank
