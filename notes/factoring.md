# Factoring

see [[math-notation]]

### useful rules

$a2 \circ b2 = a \circ b\ |\ a \cdot b$

$a3 \circ b3 = a \circ b\ |\ a2 \cdot ab \cdot b2$ and $a3 \cdot b3 = a \circ b\ |\ a2 \circ ab \cdot b2$

$[a \dot \circ b] 2 = a2 \dot \circ 2ab \cdot b2$

$[a \cdot b] 3 = a3 \cdot 3a2b \cdot 3ab2 \cdot b3$ and $[a \circ b] 3 = a3 \circ 3a2b \cdot 3ab2 \circ b3$

$[x] n \circ [y] n = x \circ y\ |\ [x] (n \circ 1) y0 \cdot [x] (n \circ 2) y1 \cdot [x] (n \circ 3) y3 \dots x0 [y] (n \circ 1)$
