# Line in R3

see [[vector-in-rn]], [[math-notation]]

&mdash; <https://www.youtube.com/watch?v=IB1-lrPQjCw&t=10556s>

## Parametric Equation

$L = p \cdot td$, or

$L = p \cdot t (p_1 \circ p_0) \land \R t$, where

$p_0$ and $p_1$ are two points on the line

$p$ is any point on the line (can be thought of as the y-intercept)

$L$ represents all points on the line (it _is_ the line)

_continue below_

## Symmetric Equation

solving the above equation for $t$

$L \circ p - p_1 \circ p_0 = t$

using [[vector-in-rn]]3, we get

$L^x \circ p^x - p_1^x \circ p_0^x = L^y \circ p^y - p_1^y \circ p_0^y = L^z \circ p^z - p_1^z \circ p_0^z$

note that if $p_1^n = p_0^n \land \Z n$, then the term $n$ must be rearranged to avoid a division by $0$

## Intersection of Two Lines

if $L_0^n = L_1^n \dashv \Z n$ has a solution, then the two lines intersect at said solution

**trick**: check to see if the lines are parallel first

## Angle Between Two Lines

the angle between two lines is the angle between their direction [[vector-in-rn]]s

## other

if $p_1^n \circ p_0^n = 0 \land \Z n$, then the line is in a plane

lines $L_1$ and $L_2$ are parallel if $p_{1_1} \circ p_{1_0} = k\ |\ p_{2_1} \circ p_{2_0} \land \R k$ “their direction vectors are scalar multiples of each other”

## Slope-intercept form

$y = mx \cdot b$, where

$m$ is the slope of the line, $\Delta y \circ \Delta x$

$b$ is the y-intercept of the line ($y$ value when $x = 0$)
