# Productivity

_The quantity of goods and services produced per unit of a worker’s time_

see [[wealth]], [[economic-growth]]

## importance of productivity

- productivity and living standards are closely related

## determining productivity

> **Physical Capital**: the stock of equipment and structures that are used to produce [[goods-and-services]]

> **Human Capital**: the knowledge and skills that workers acquire through education, training and experience

> **Natural Resources**: the inputs into the production of [[goods-and-services]] that are produced by nature, such as land, river and mineral deposits

> **Technological Knowledge**: society’s understanding of the best way to produce [[goods-and-services]]
