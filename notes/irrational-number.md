# Irrational Number

see [[math-notation]]

$\mathbb{Q}^* \vdash \R$

## definition

$\mathbb{Q}^* x \equiv \lnot \mathbb{Q} x \land \R x$
