# Span

see [[linear-combination]]

the set of all [[linear-combination]]s of the [[vector]]s $u_0 \dots u_m$ is called the _span_

$u_0 \dots u_m$ is a _spanning set_ for the resulting [[vector-space]]

$u_0 \dots u_m$ _span_ the resulting [[vector-space]]

### properties

a spanning set is always a [[vector-space]], and all [[vector-space]]s can be represented as a spanning set

### notation

$Span\{u_0 \dots u_m\} \dashv \R^nu \dashv \Z n$

$Vec$ is the French notation whereas $Span$ is the English notation
