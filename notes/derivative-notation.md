# Derivative Notation

see [[classical-math-notation]]

## definition

$\frac{df}{dx} = f'(x) = \lim_{h \to 0} \frac{f(x + h) - f(x)}{h} = \lim_{x \to a} \frac{f(x) - f(a)}{x - a}$

## Normal Notation

first derivative: $f'(x)$

second derivative: $f''(x)$

third derivative: $f'''(x) = f^{(3)}(x)$

nth derivative: $f^{(n)}(x)$

## Leibniz's Notation

first derivative: $\frac{d}{dx}f(x) = \frac{df}{dx}$

nth derivative: $\frac{d^n}{dx^n} = \frac{d^nf}{dx^n}$
