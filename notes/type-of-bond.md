# Type of Bond

see [[bond]]

_there are three different main kinds of bonds_

## Ionic Bond

### properties

- between two atoms, one metal and one non-metal
- involves the transfer of electrons: one has gained electrons, one has lost electrons
- 2 opposite charges attract, forming a bond

### substance properties

- results in the formation of ionic solids such as crystals
- very stable, high melting points, boiling points, rigid
- they cleave along a line
- often very soluble in polar solvents such as water

## Metallic Bond

### properties

- between metals
- a surface (sea) of electrons allows for great conductivity
- bonds are very strong

### substance properties

- they tend to deform, be very malleable and ductile
- very high melting and boiling points

## Covalent Bond

### properties

- between non-metals
- involves sharing of electrons
- one bond consists of 2 electrons shared between two atoms, not necessarily equally, see [[electronegativity]]

### substance properties

- a lot more colour
- tend to be brittle
- molecular substances, we could theoretically isolate a single molecule
- generally have reasonably strong intermolecular bonds, but weak intramolecular bonds
- low melting points, low density, low boiling points
