# Classical Math Notation

see [[math-notation]], [[java]], [[java-is-a-terrible-language]]

## Problems with [[classical-math-notation]]

_fixed in my [[math-notation]]_

- { | } notation is just a ∩
- √ only returns a positive solution
- ± and | | are totally different but do effectively the same thing
- ab = a \* b, why multiplication?
- division written as long bar but not multiplication
- 4², √4, log2 are the same operation with completely different notation
- no conditional equality symbol
- matrices and vectors can be ambiguous
- variables are one letter only, but oh well
- why not allow 2D notation?
- chained inequalities don't exist
- ^-1 means both reciprocal and inverse, eg. sin-1(x) is a reciprocal where it should be an inverse
- complex numbers are not considered most of the time (sqrt of negative and log of zero are often "undefined")
- nested BRACKETS
- exponents are right-associative
- functions can only take one parameter
- indices start at 1
- ∈ and sets
- a(b) is ambiguous: both multiplication and composition
- points and vectors in $\R^n$ are two different concepts
