# Significant Figures

see <http://cxp.cengage.com/contentservice/assets/owms01h/references/significantfigures/index.html>, [[classical-math-notation]]

## Rule

> All digits are significant, except for leading zeros or for zeros that appear after a number without being followed by a decimal point.

$3.0$ has 2 significant figures

$30$ has 1 significant figure

$30.$ has 2 significant figures

$0.005$ has 1 significant figure

$0.0050$ has 2 significant figures

## Operations

assuming a base-10 counting system and base-10 logarithms

### Addition and Subtraction

keep as many significant figures after the last significant digit as the lowest amount of significant figures after the last significant digit in the original numbers

$\ \ \ \ \ \ 530$ (2 significant figures)

$+\ 1140.5$ (5 significant figures, offset)

$=1190$ (3 significant figures, trailing digits cut off)

### Multiplication and Division

_keep as many significant figures as the number with the least number of significant figures_

### Logarithms

_keep as many significant figures to the right of the decimal point as there are significant figures in the original number_

$\log_{10} 4.000$ (4 significant figures)

$= 0.6021$ (4 significant figures on the right of the decimal point)

### Antilogarithms

_keep the same number of significant figures as the number of significant figures after the decimal point in the original number_

$10^{-2.55}$ (2 significant figures on the right of the decimal point)

$= 2.8\times 10^{-3}$ (2 significant figures)
