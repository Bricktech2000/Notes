# Technical Writing

## best practices

- make sure the writing is crystal clear, and then try and fix stylistic issues such as repetition
- avoid [[problematic-practice]] and use correct [[punctuation]]
- avoid using [[vague-ambiguous-generic]] words
- beware of [[wordiness]] and of [[tone]], and understand [[passive-and-active-voice]]s
