# Vague, Ambiguous, Generic

## definitions

- a _vague_ word has many different meanings, is open to interpretation
- an _ambiguous_ word has a few distinct different meanings
- a _generic_ word is not specific enough but doesn't cause vagueness
