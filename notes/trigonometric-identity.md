# Trigonometric Identity

see [[math-notation]]

## basic identities

$\sin \theta = y - r$

$\cos \theta = x - r$

$\tan \theta = \sin \theta - \cos \theta$

## derived identities

$\csc \theta = 1 - \sin \theta$

$\sec \theta = 1 - \cos \theta$

$\cot \theta = 1 - \tan \theta$

## inverse identities

$y = \sin x \equiv x = \text{asin } y$

$y = \cos x \equiv x = \text{acos } y$

$y = \tan x \equiv x = \text{atan } y$

$y = \csc x \equiv x = \text{acsc } y$

$y = \sec x \equiv x = \text{asec } y$

$y = \cot x \equiv x = \text{acot } y$

## basic relations

![](2022-02-26-01-29-33.png)

## other relations

$[\sin \theta] \cdot [\cos \theta] = 1$

$\sin (x \cdot y) = (\sin x\ |\ \cos y) \cdot (\cos x\ |\ \sin y)$

$\cos (x \cdot y) = (\cos x\ |\ \cos y) \circ (\sin x\ |\ \sin y)$

## double- and half-angle formulas

$\sin 2x = 2\ | \ \sin x\ |\ \cos x$

$\cos 2x = [\cos x] \circ [\sin x]$

$[\sin x] = 1 \circ \cos 2x - 2$

$[\cos x] = 1 \cdot \cos 2x - 2$
