# English Bad Habits

_a few of my English bad habits_

## Correct

> What's that called

> Wish me luck

> Profs are profs and teachers are teachers

> pronunciation of pronunciation

> r**ec**tangle

> dis**k**usting

> when I see you again (🎵)

> dis**tri**bute

> tran**sis**tor

> dis**tri**buting

## Incorrect

> How's that called

> Wish me good luck

> when I’m going to see you again (🎵)

## Careful

> This VS That
