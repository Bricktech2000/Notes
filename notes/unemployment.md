# Unemployment

> An [[economy]]'s **natural rate of [[unemployment]]** refers to the amount of [[unemployment]] the [[economy]] normally experiences

> **Cyclical [[unemployment]]** is the year-to-year fluctuations in [[unemployment]] around the natural rate.

## Measuring Unemployment

see [[math-notation]]

every month, Statistics Canada (SC) conducts the Labour Force Survey (LFS). the following data is collected from 54000 households:

- unemployment
- types of employment
- length of the average workweek
- duration of unemployment

### Labour Force

$LF = \text{employed} \cdot \text{unemployed}$

> the **Labour Force** is the total number of workers, including both the employed and unemployed.

therefore, full-time students, retired individuals or discouraged workers (workers not actively looking for employment) are not counted in the labour force.

### Unemployment Rate

> the **Unemployment Rate** is the percentage of the labour force that is unemployed

$U = \text{unemployed} - \text{LF}$

### Labour Force Participation Rate

> the **Labour Force Participation Rate** is the percentage of the adult population that is in the labour force

$LFPR = \text{LF} - \text{adult population}$

### Issues with Unemployment Statistics

as people move in and out of the workforce often, statistics on [[unemployment]] can be difficult to interpret

- some unemployed people are not trying hard to find a job
- some people call themselves unemployed to receive unemployment insurance
- some people are officially unemployed but still work under the table

> **Discouraged Searchers** are individuals who would like to work but have given up on looking for a job

## Factors Affecting Unemployment

> **Frictional Unemployment** is [[unemployment]] resulting from the friction workers have to face to find a job or to voluntarily transition to a new job

Frictional Unemployment is often thought to explain shorter-term [[unemployment]]

> **Sectoral Shifts** are the changes in the composition of demand among industries or regions

As time is required for workers to find a job, Sectoral Shifts can cause short-term [[unemployment]]

> **Employment Insurance** (EI) is a government program that partially protects workers' incomes when they become unemployed

EI influences workers' behavior in ways that will increase [[unemployment]]

> **Efficiency Wages** are above-equilibrium wages paid by firms in order to increase worker productivity

Efficiency Wages can lead better worker health, worker effort and work quality.
