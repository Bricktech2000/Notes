# Ideal Gas

see [[stoichiometry]], [[classical-math-notation]]

> An ideal gas is a theoretical gas composed of many randomly moving point particles that are not subject to interparticle interactions. &mdash; Wikipedia

## Ideal Gas Law

$PV = nRT$, where

$P$ is the pressure of the gas

$V$ is the volume of the gas

$n$ is the number of moles of gas present

$R$ is the ideal gas constant

$T$ is the absolute temperature of the gas

## Ideal Gas Constant

$R = 8.31451 \frac{J}{mol K} = 8.31451 \frac{L \cdot kPa}{mol K} = 0.08314 \frac{L \cdot bar}{mol K}$

## Dalton's Law

> Dalton's law states that in a mixture of non-reacting gases, the total pressure exerted is equal to the sum of the partial pressures of the individual gases. &mdash; Wikipedia

$P = \sum_i P_i$, where

$P$ is the total pressure

$P_i$ is the partial pressure of each gas
