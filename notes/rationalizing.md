# Rationalizing

_when a square root is in the denominator_

see [[math-notation]]

### useful rules

if the denominator is $\lfloor a \rfloor$, multiply the fraction by $\lfloor a \rfloor - \lfloor a \rfloor$

if the denominator is $\lfloor a \rfloor \dot \circ \lfloor b \rfloor$, multiply the fraction by $\lfloor a \rfloor \mathring \cdot \lfloor b \rfloor - \lfloor a \rfloor \mathring \cdot \lfloor b \rfloor$
