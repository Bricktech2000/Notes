# Extremum

see [[inflection-point]], [[classical-math-notation]]

> extrema (the plural of extremum), are the largest and smallest value of the function, either within a given range (the local or relative extrema), or on the entire domain (the global or absolute extrema). &mdash; Wikipedia

$f'(x) = 0$

## Second Derivative Test

_verifies whether an extremum is a maximum or a minimum_

- the point is a minimum if $f''(x) > 0$
- the point is a maximum if $f''(x) < 0$
