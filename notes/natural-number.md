# Natural Number

see [[math-notation]]

$\N \vdash \Z$

## definition

_a whole, non-negative number_

**note**: $\N 0$
