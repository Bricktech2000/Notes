# Formal Charge

see [[bond]], [[classical-math-notation]]

## equation

for any atom,

$FC = FC_{atom} = \text{valence electrons} - \text{lone electrons} - \text{bonds}$, where

$\text{valence electrons}$ is the number of valence electrons on a neutral atom

$\text{lone electrons}$ is the number of lone electrons in the molecule (double the number of lone pairs)

$\text{bonds}$ is the number of bonds on the atom (a double bond would have a value of $2$)

## Formal Charge of a molecule

the total formal charge must equal the charge of the molecule or ion. therefore, for any neutral molecule,

$FC_{total} = \sum FC_{atom} = 0$

## Preferred Formal Charge

see [[drawing-a-molecule]]

1. the formal charge must sum to the charge of the molecule or ion
2. a $0$ formal charge is preferred on an atom, otherwise, a smaller number is preferred
3. atoms with higher [[electronegativity]] prefer a smaller formal charge
