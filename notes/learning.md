# 🅻🅴🅰🆁🅽🅸🅽🅶

[[sleep]], [[insomnia]]

[[less-stimulated-lifestyle]]

[[recollection-vs-recognition]]

[[environmental-dependency-of-learning]]

[[concept-vs-fact]]

[[english-bad-habits]]

[[surface-volume-disambiguation]]

an excess of [[motivation]] can lead to [[feeling-overwhelmed]], which can lead to [[procrastination]]

[[desire-vs-value]]

[[hijacking-brain-circuitry]]

[[curve-fitting]]

[[mnemonics]]

[[intent-to-mastery]]

[[credit-card]]
