# Concept VS Fact

for better [[learning]], identify if what you're learning is a concept or a fact.

&mdash; <https://youtu.be/IlU-zDU6aQ0?t=1264>

## Fact

_an isolated piece of information related to a concept_

facts can easily get confused, which is the reason why we have reference books and search engines. _if you know the concept, you can quickly look up the fact_. facts can be learned through [[mnemonics]].

### examples

- a date at which an event took place
- the name of a bone in the human body

## Concept

_a piece of information tightly related to multiple other concepts_

generally, they are more important than facts. they can be structured through conceptual [[README|Notes]]. once grasped, they stay with you a lifetime. when learning concepts, make sure to understand the difference between [[recollection-vs-recognition]].

### examples

- understanding the function of a bone in the human body
- understanding why an event actually took place
