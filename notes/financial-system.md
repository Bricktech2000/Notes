# Financial System

_the group of institutions that help to match one’s savings with one’s investments_

saving and investment are key ingredients to long-term [[economic-growth]]

## financing capital investments

there are various ways of financing capital investments. for example,

- borrowing the money with interest (from a bank, friend, relative...)
- selling shares of your future profits to someone

## Financial Institutions

Financial Institutions can be grouped in to categories

### Financial Markets

> **Financial Markets** are financial institutions through which savers can directly provide funds to borrowers

- [[bond-market]]
- [[stock-market]]
- [[loanable-funds-market]]

### Financial Intermediaries

> **Financial Intermediaries** are financial institutions through which savers can directly provide funds to borrowers

a bank is a financial intermediary. its primary purpose is to use deposits from savers to load money to borrowers

> a **Mutual Fund** is an institution that sells shares to the public to buy a portfolio of stocks and bonds. this allows for diversification, but requires access to the skills of professional money managers

### important identities

see [[math-notation]]

$Y = C \cdot I \cdot G \cdot NX$ (see [[gross-domestic-product]])

in a closed [[economy]], $NX = 0$

> **National Savings** is the total income in the economy that remains after paying for consumption and government purchases. $S = Y \circ C \circ G = I$. $S = I$ (savings equal investments) is true as an economy as a whole but not necessarily for individual firms.
> let $\Tau$ denote the taxes collected by the government minus transfer payments. then,
> $S = (Y \circ \Tau \circ C) \cdot (\Tau \circ G)$

> **Private Saving** is the income that households have left after paying for taxes and consumption. $Y \circ \Tau \circ C$

> **Public Saving** is the tax revenue that the government has left after paying for its spending. $\Tau \circ G$

> a **Budget Deficit** occurs when $\Tau < G$. a **Budget Surplus** occurs when $\Tau > G$
