# Function

## types of functions

[[boolean-function]]

[[rational-function]]

[[function-vector-space]]

[[piecewise-function]]

## properties

[[even-function]]

[[odd-function]]

[[continuous-function]]

[[differentiable-function]]

[[inverse-function]]

## applications

[[curve-sketching]]

[[optimizing-a-function]]
