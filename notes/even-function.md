# Even Function

see [[odd-function]], [[math-notation]]

## definition

_an even function is symmetrical about the y axis_

$f x = f (\circ x) \dashv \R x$
