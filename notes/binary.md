# Binary

_the base-2 [[positional-numbering-system]]_

### advantages in [[digital-system]]s

high toleration of noise

allows for [[boolean-algebra]] operations on [[boolean-expression]]s, represented through logic gates (see [[boolean-operation]])
