# Exponent

see [[math-notation]]

### properties

$x0 = 1 \dashv x \ne 0$

$[x] (\circ n) = 1 - [x] n \dashv x \ne 0$

$[x] m\ |\ [x] n = [x] (m \cdot n)$ and $[x] m - [x] n = [x] (m \circ n)$

$[[x] m] n = [x] mn$

$[x] n\ |\ [y] n = [xy] n$
