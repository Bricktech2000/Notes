# Implicit Limits

Would there be any disadvantage to having every mathematical expression be implicitly evaluated using limits?

Is there any situation in which an undefined value would be required?

This would allow for any algebraic manipulation on expressions without having to worry about restrictions.

Update: the following video breaks the principle stated above.

<https://www.youtube.com/watch?v=hI9CaQD7P6I&ab_channel=JohnHush>

$\lim_{x \to 0}\frac x x$

$\frac{\lim_{x \to 0} x}{x}$
