# Electromagnetic Wave

see [[wavelength]], [[frequency]], [[energy]], [[electron]], [[photon]], [[light]], [[avogadros-number]], [[specific-heat]], [[planks-constant]], [[density]], [[classical-math-notation]]

a particle of light is called a [[photon]]

_generally, one [[photon]] ejects one [[electron]]_

_for [[chm1301-principles-of-chemistry]], assume $1mol_{e^-}$ = $1mol_{substance}$\_

## Formulas

### Wave Equation

$\nu\lambda = c$, where

$c$ is the speed of [[light]]

$\nu$ is the [[frequency]] of the wave

$\lambda$ is the [[wavelength]] of the wave

### Planck–Einstein Relation

$E = h\nu$, where

$E$ is the [[energy]] carried by one [[photon]] in $J$

$h$ is [[planks-constant]]

$\nu$ is the [[frequency]] of the wave

### Photoelectric Effect

$KE = h\nu - \phi$, where

$KE$ is the kinetic [[energy]] of the ejected [[electron]] in $J$

$h\nu$ is the [[energy]] of the incident [[light]]

$\phi$ is the binding energy or work function (the energy holding an electron to an atom) in $J/mol_{e^-}$

### other equations

$E_T = E_{photon} \times \#_\gamma$, where

$E_T$ is the total [[energy]] in the wave, see [[specific-heat]]

$E_{photon}$ is the [[energy]] carried by one [[photon]]

$\#_\gamma$ is the number of [[photon]]s

## Constants

$1nm = 1 \times 10^{-9}m$
