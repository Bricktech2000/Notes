# Diminishing Returns

see [[economics]]

> **Diminishing Returns**: the benefit from an additional unit of input declines as the quantity of input increases
