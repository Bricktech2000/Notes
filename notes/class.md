# Class

_the blueprint of an [[object]]_

an [[object]] has a state, whereas a Class does not

### definitions

> two classes are said to be **coupled** if the implementation of one of them depends on the implementation of the other. this is to be avoided

## Java Classes

see [[java]]

### contains

see [[object]]

- properties
- methods
- nested [[class]]es

which can all either be:

- static or not (belong to the [[class]] or to an instance or [[object]])
- public, (no value, package), protected or private
- final or not

### definitions

> a **final** variable can only be initialized once

### note

> the order in which [[class]] properties and methods are defined in [[java]] is irrelevant
