# Avogadro's Number

see [[classical-math-notation]]

Avogadro's number: $6.022 \times 20^{23} \frac{1}{mol}$
