# Calculating a Derivative

see [[derivative-rules]], [[classical-math-notation]], [[limit-rules]], [[calculating-a-limit]], [[logarithmic-differentiation]]

## Differentiation

apply [[derivative-rules]] recursively. see [[logarithmic-differentiation]]

## Implicit Differentiation

> Derivation of an implicit equation (where the dependent variable is not isolated). Works with both functions and relations that aren't functions.

### example

$x^2 + y^2 = 2$

$\frac{d}{dx}(x^2 + y^2) = \frac{d}{dx}(2)$

$2x + 2y \times \frac{dy}{dx} = 0$

$\frac{dy}{dx} = -\frac{x}{y}$
