# Effusion

see [[classical-math-notation]]

> In physics and chemistry, effusion is the process in which a gas escapes from a container through a hole of diameter considerably smaller than the mean free path of the molecules [a very small hole] &mdash; Wikipedia

## relative effusion of two gases

$\frac{\text{rate of effusion of A}}{\text{rate of effusion of B}} = \sqrt{\frac{MM_B}{MM_A}} = \frac{\text{time of effusion of B}}{\text{time of effusion of A}}$
