# Catch-up Effect

see [[economics]]

> **Catch-up Effect**: entities (countries) that start off poor tend to grow more rapidly than entities (countries) that start out relatively rich
