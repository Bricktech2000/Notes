# Normal Vector

symbol: $n$

a [[vector]] perpendicular to a plane

it can be found using the [[cross-product]] of two vectors in a [[plane-in-r3]]
