# Punctuation

## best practices

- _course's objective_ is awkward to say (uses possessive grammar) whereas _course objective_ is better (uses an adjective)
- the serial comma (Oxford comma) before an _and_ at the end of a list is not wrong (but not required)
- use vivid, descriptive, subject-specific language (avoid _good, thing, etc._)
- generally, a semicolon `;` is used instead of a period to indicate that two sentences are closely related
- a `-` is a [[hyphen]] (_well-written_) whereas a `--` is a [[dash]] (_his report is excellent &mdash; clear, concise and informative_)

## Run-on Sentence

> A run-on sentence results from two or more sentences being connected without any punctuation. &mdash; <https://msvu.ca>
