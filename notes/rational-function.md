# Rational Function

see [[classical-math-notation]]

> basically, a [[polynomial]] divided by a different [[polynomial]]

$f(x) = \frac{numerator}{denominator}$

## Vertical Asymptote

- when $denominator = 0$
- calculate using $\lim_{denominator \to 0} f(x)$

## Horizontal Asymptote

- when $x$ approaches infinity
- calculate using $\lim_{x \to \pm \infty} f(x)$, see [[calculating-a-limit]]

## Holes

- when $numerator = 0$ and $denominator = 0$
- simplify the function to calculate
